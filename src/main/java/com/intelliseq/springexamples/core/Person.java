package com.intelliseq.springexamples.core;

public class Person {
	
	private String firstName;
	private String familyName;
	
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public void setFamilyName(String familyName) {
		this.familyName = familyName;
	}

	@Override
	public String toString() {
		return firstName + " " + familyName;
	}
}
