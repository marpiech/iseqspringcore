package com.intelliseq.springexamples.core;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class SpringApp {

	public static void main (String[] args) {
		ApplicationContext context =
			    new ClassPathXmlApplicationContext(new String[] {"com/intelliseq/springexamples/application-context.xml"});
		Person person = (Person) context.getBean("person");
		System.out.println(person);
		Person modernPerson = (Person) context.getBean("person-modern");
		System.out.println(modernPerson);
	}
	
}
